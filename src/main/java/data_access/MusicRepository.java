package data_access;
import models.*;

import java.sql.*;
import java.util.ArrayList;

public class MusicRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    public ArrayList<Artist> getRandomArtists(){
        // Get 5 random artists from DB.
        ArrayList<Artist> randomArtists = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT * FROM artist ORDER BY RANDOM() LIMIT 5;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                randomArtists.add(
                        new Artist(
                                result.getString("ArtistId"),
                                result.getString("Name")
                        ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return randomArtists;
    }

    public ArrayList<Song> getRandomSongs(){
        // Get 5 random artists from DB.
        ArrayList<Song> randomSongs = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT trackid, name FROM track ORDER BY RANDOM() LIMIT 5;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                randomSongs.add(
                        new Song(
                                result.getString("TrackId"),
                                result.getString("Name")
                        ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return randomSongs;
    }

    public ArrayList<Genre> getRandomGenres(){
        // Get 5 random artists from DB.
        ArrayList<Genre> randomGenres = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT genreid, name FROM genre ORDER BY RANDOM() LIMIT 5;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                randomGenres.add(
                        new Genre(
                                result.getString("GenreId"),
                                result.getString("Name")
                        ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return randomGenres;
    }

    public ArrayList<Song> searchForSong(String searchTerm){
        // Get 5 random artists from DB.
        ArrayList<Song> searchResults = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            String query = "SELECT " +
                    "track.name AS trackName, " +
                    "album.title AS albumTitle, " +
                    "artist.name AS artistName, " +
                    "genre.name AS genreName " +
                    "FROM " +
                    "track " +
                    "INNER JOIN album ON album.albumid = track.albumid " +
                    "INNER JOIN artist ON artist.artistid = album.artistid " +
                    "INNER JOIN genre ON genre.genreid = track.genreid " +
                    "WHERE track.name LIKE ";

            PreparedStatement pStatement =
                    //conn.prepareStatement("SELECT genreid, name FROM track WHERE name LIKE ?");
                    conn.prepareStatement(query + "?");
                    pStatement.setString(1,"%" + searchTerm + "%");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                searchResults.add(
                        new Song(
                                result.getString("trackName"),
                                result.getString("artistName"),
                                result.getString("albumTitle"),
                                result.getString("genreName")
                        ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return searchResults;
    }

}
