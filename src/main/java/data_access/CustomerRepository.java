package data_access;

import models.Country;
import models.Customer;
import models.CustomerWithSpendings;
import models.MostPopularCustomerGenre;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    // All customers
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers= new ArrayList();
        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT customerid, firstname, lastname, country, postalcode, phone " +
                            "FROM customer;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                        customers.add(new Customer(
                                result.getString("customerid"),
                                result.getString("firstname"),
                                result.getString("lastname"),
                                result.getString("country"),
                                result.getString("postalcode"),
                                result.getString("phone")
                        ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return customers;
    }

    // Add new Customer
    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            // Query
            PreparedStatement pStatement =
                    conn.prepareStatement("INSERT INTO " +
                            "customer(customerid, firstname, lastname, company, address, " +
                            "city, state, country, postalcode, phone, fax, email, supportrepid) "  +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pStatement.setString(1, customer.getId());
            pStatement.setString(2, customer.getFirstName());
            pStatement.setString(3, customer.getLastName());
            pStatement.setString(4, customer.getCompany());
            pStatement.setString(5, customer.getAddress());
            pStatement.setString(6, customer.getCity());
            pStatement.setString(7, customer.getState());
            pStatement.setString(8, customer.getCountry());
            pStatement.setString(9, customer.getPostalCode());
            pStatement.setString(10, customer.getPhone());
            pStatement.setString(11, customer.getFax());
            pStatement.setString(12, customer.getEmail());
            pStatement.setString(13, customer.getSupportRepId());

            int result = pStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Customer added.");

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return success;
    }

    // Update customer
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            // Query
            PreparedStatement pStatement =
                    conn.prepareStatement("UPDATE customer set customerid=?, firstname=?," +
                            "lastname=?, company=?, address=?, city=?, state=?, country=?," +
                            "postalcode=?, phone=?, fax=?, email=?, supportrepid=?" +
                            " WHERE customerid=?");
            pStatement.setString(1, customer.getId());
            pStatement.setString(2, customer.getFirstName());
            pStatement.setString(3, customer.getLastName());
            pStatement.setString(4, customer.getCompany());
            pStatement.setString(5, customer.getAddress());
            pStatement.setString(6, customer.getCity());
            pStatement.setString(7, customer.getState());
            pStatement.setString(8, customer.getCountry());
            pStatement.setString(9, customer.getPostalCode());
            pStatement.setString(10, customer.getPhone());
            pStatement.setString(11, customer.getFax());
            pStatement.setString(12, customer.getEmail());
            pStatement.setString(13, customer.getSupportRepId());
            pStatement.setString(14, customer.getId());

            int result = pStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Customer updated.");

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return success;
    }

    // Countries and number of customers for each country, sorted descending
    public ArrayList<Country> getNoOfCustomers(){
        ArrayList<Country> countries = new ArrayList();
        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT country, COUNT(country) AS noOfCustomers " +
                            "FROM customer GROUP BY country ORDER BY noOfCustomers DESC;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                countries.add(new Country(
                        result.getString("country"),
                        result.getString("noOfCustomers")
                ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return countries;
    }

    // Customers with total spendings, sorted descending
    public ArrayList<CustomerWithSpendings> getCustomersWithSpendings(){
        ArrayList<CustomerWithSpendings> customersWithSpendings = new ArrayList();
        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement("SELECT customer.customerid, customer.firstname, " +
                            "customer.lastname, SUM(invoice.total) as spendings " +
                            "FROM customer INNER JOIN invoice ON customer.customerid = invoice.customerid " +
                            "GROUP BY invoice.customerid " +
                            "ORDER BY spendings DESC;");

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                customersWithSpendings.add(new CustomerWithSpendings(
                        result.getString("customerid"),
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("spendings")
                ));
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return customersWithSpendings;
    }

    // Most popular genre for specific customer
    public MostPopularCustomerGenre getCustomerMostPopularGenre(String id){
        MostPopularCustomerGenre mostPopularGenre = null;
        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established.");

            PreparedStatement pStatement =
                    conn.prepareStatement(
                            "SELECT customer.customerid, customer.firstname, customer.lastname, genre.name," +
                                    " COUNT(*) as count" +
                                    " FROM customer" +
                                    " INNER JOIN invoice ON invoice.customerid = customer.customerid" +
                                    " INNER JOIN invoiceline ON invoiceline.invoiceid = invoice.invoiceid" +
                                    " INNER JOIN track ON track.unitprice = invoiceline.unitprice" +
                                    " INNER JOIN genre ON genre.genreid = track.genreid" +
                                    " WHERE customer.customerid =?" +
                                    " GROUP BY genre.name" +
                                    " ORDER BY count DESC" +
                                    " LIMIT 1"
                    );
            pStatement.setString(1, id);

            ResultSet result = pStatement.executeQuery();

            while(result.next()) {
                mostPopularGenre = new MostPopularCustomerGenre(
                        result.getString("customerid"),
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("name")
                );
            }

        } catch(SQLException s){
            System.err.println(s.getMessage());

        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                System.err.println(e.getMessage());
            }
        }
        return mostPopularGenre;
    }

}
