package models;

public class Artist {
    private String artistId;
    private String artistName;

    public Artist(String ID, String name){
        this.artistId = ID;
        this.artistName = name;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getArtistName() {
        return artistName;
    }
}
