package models;

public class Genre {

    private String genreId;
    private String genreName;

    public Genre(String ID, String name){
        this.genreId = ID;
        this.genreName = name;
    }

    public String getGenreId() {
        return genreId;
    }

    public String getGenreName() {
        return genreName;
    }
}
