package models;

public class CustomerWithSpendings {
    private String id;
    private String firstName;
    private String lastName;
    private String spendings;

    public CustomerWithSpendings(String id, String firstName, String lastName, String spendings) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.spendings = spendings;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSpendings() {
        return spendings;
    }
}
