package models;

public class Country {
    private String country;
    private String noOfCustomers;

    public Country(){}

    public Country(String country, String noOfCustomers) {
        this.country = country;
        this.noOfCustomers = noOfCustomers;
    }

    public String getCountry() {
        return country;
    }

    public String getNoOfCustomers() {
        return noOfCustomers;
    }
}
