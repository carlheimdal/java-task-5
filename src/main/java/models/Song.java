package models;

public class Song {
    private String songId;
    private String songName;
    private String artist;
    private String album;
    private String genre;

    public Song(String ID, String name){
        this.songId = ID;
        this.songName = name;
    }

    public Song(String name, String artist, String album, String genre){
        this.songName = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }

    public String getSongId() {
        return songId;
    }

    public String getSongName() {
        return songName;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getGenre() {
        return genre;
    }
}
