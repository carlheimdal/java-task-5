package models;

public class MostPopularCustomerGenre {
    private String customerId;
    private String firstName;
    private String lastName;
    private String mostPopularGenre;

    public MostPopularCustomerGenre(){}

    public MostPopularCustomerGenre(String id, String firstName, String lastName, String genre){
        this.customerId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mostPopularGenre = genre;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMostPopularGenre() {
        return mostPopularGenre;
    }
}
