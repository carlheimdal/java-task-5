package controllers;

import data_access.CustomerRepository;
import models.Country;
import models.Customer;
import models.CustomerWithSpendings;
import models.MostPopularCustomerGenre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    CustomerRepository customerData = new CustomerRepository();

    // All customers
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET, produces = "application/json")
    public ArrayList<Customer> getAllCustomers(){
        return customerData.getAllCustomers();
    }

    // Number of customers by country, descending
    @RequestMapping(value = "/api/customers/country/descending", method = RequestMethod.GET, produces = "application/json")
    public ArrayList<Country> getNoOfCustomers(){
        return customerData.getNoOfCustomers();
    }

    // Customers and spendings, descending
    @RequestMapping(value = "/api/customers/spendings/descending", method = RequestMethod.GET, produces = "application/json")
    public ArrayList<CustomerWithSpendings> getCustomersWithSpendings(){
        return customerData.getCustomersWithSpendings();
    }

    // Most popular genre for given customer
    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET, produces = "application/json")
    public MostPopularCustomerGenre getCustomerMostPopularGenre(@PathVariable String id){
        return customerData.getCustomerMostPopularGenre(id);
    }

    // Add new customer
    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerData.addCustomer(customer);
    }

    // Update customer
    @RequestMapping(value = "api/customers", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer){
        return customerData.updateCustomer(customer);
    }
}
