package controllers;

import data_access.MusicRepository;
import models.Song;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class MusicController {
    MusicRepository dbHelper = new MusicRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showRandomArtists(Model model){
        model.addAttribute("artists", dbHelper.getRandomArtists());
        model.addAttribute("songs", dbHelper.getRandomSongs());
        model.addAttribute("genres", dbHelper.getRandomGenres());
        return "home";
    }

    @RequestMapping(value = "/search")
    public String searchForSong(@RequestParam(value = "searchTerm") String search, Model model){
        model.addAttribute("results", dbHelper.searchForSong(search));
        return "search";
    }
}
