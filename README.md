# WhyTunes

## Description
path "/" Shows some random things from SqLite Chinook database.
There is a search bar where you can search for songs.

## Postman
Postman collection located in root dir

## Heroku
https://sheltered-spire-32148.herokuapp.com/

## Api endpoints

### GET
- /api/customers returns all customers in database
- /api/customers/country/descending shows countries with number of customers, sorted descending
- /api/customers/spendings/descending shows customers with their spending, sorted descending
- /api/customers/:customerid/popular/genre Shows customer and their most popular genre

### POST/PUT
POST or PUT a customer to /api/customers to add or update a customer.